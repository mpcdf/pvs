program testmpi
  use mpi
  !$ use omp_lib

  implicit none
  integer mpi_size, mpi_rank, error
  integer :: n
  real :: cpu_start, cpu_end, t, random

  call cpu_time(cpu_start)

  call mpi_init(error)

  call mpi_comm_size(MPI_COMM_WORLD, mpi_size, error)
  call mpi_comm_rank(MPI_COMM_WORLD, mpi_rank, error)

  if (mpi_rank == 0) then
    write(*,*) "tasks", mpi_size
    !$OMP PARALLEL
    !$ if (omp_get_thread_num() == 0) then
    !$   write(*,*) "threads", omp_get_num_threads()
    !$ endif
    !$OMP END PARALLEL
  endif

  call cpu_time(random)
  do n = 1, 10000000
    call cpu_time(t)
    random = cos(random + t)
  end do

  if (mpi_rank == 0) then
    call cpu_time(cpu_end)
    write(*,*) "cpu_time", cpu_end - cpu_start
    write(*,*) "random", random
  endif

  call mpi_finalize(error)
end program
